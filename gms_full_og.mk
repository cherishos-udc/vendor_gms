#
# Copyright (C) 2018-2019 The Google Pixel3ROM Project
# Copyright (C) 2024 The hentaiOS Project and its Proprietors
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

# Quick Tap
ifeq ($(TARGET_SUPPORTS_QUICK_TAP), true)
PRODUCT_PACKAGES += \
    quick_tap
endif

# product/app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    Chrome \
    Chrome-Stub \
    DevicePolicyPrebuilt \
    Drive \
    GeminiPrebuilt \
    GoogleContacts \
    GoogleTTS \
    LatinIMEGooglePrebuilt \
    LocationHistoryPrebuilt \
    Maps \
    MarkupGoogle \
    MeetPrebuilt \
    NgaResources \
    Photos \
    PixelThemesStub \
    PixelThemesStub2022_and_newer \
    PixelWallpapers2023 \
    PlayAutoInstallConfig \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail \
    SoundAmplifierPrebuilt \
    SoundPickerPrebuilt \
    SwitchAccessPrebuilt \
    TrichromeLibrary \
    TrichromeLibrary-Stub \
    Tycho \
    Videos \
    VoiceAccessPrebuilt \
    WallpaperEmojiPrebuilt \
    WebViewGoogle \
    WebViewGoogle-Stub \
    arcore \
    talkback

ifneq ($(filter google, $(PRODUCT_BRAND)),)
PRODUCT_PACKAGES += \
    GoogleCamera \
    SCONE
endif

ifneq ($(filter flame coral redfin oriole raven panther cheetah lynx felix shiba husky, $(TARGET_DEVICE)),)
PRODUCT_PACKAGES += \
    DreamlinerPrebuilt \
    DreamlinerUpdater
endif

ifneq ($(filter felix, $(TARGET_DEVICE)),)
PRODUCT_PACKAGES += \
    PixelWallpapers2023Foldable
endif

ifneq ($(filter shiba husky, $(TARGET_DEVICE)),)
PRODUCT_PACKAGES += \
    DevicePersonalizationPrebuiltPixel2023
else
PRODUCT_PACKAGES += \
    DevicePersonalizationPrebuiltPixel2022
endif

# product/priv-app
PRODUCT_PACKAGES += \
    AdaptiveVPNPrebuilt \
    AICorePrebuilt \
    AiWallpapers \
    AmbientStreaming \
    AndroidAutoStubPrebuilt \
    BetterBugStub \
    CarrierLocation \
    CarrierMetrics \
    CbrsNetworkMonitor \
    ConfigUpdater \
    DeviceIntelligenceNetworkPrebuilt \
    FilesPrebuilt \
    GCS \
    GoogleDialer \
    GoogleOneTimeInitializer \
    GoogleRestorePrebuilt \
    HealthIntelligenceStubPrebuilt \
    KidsSupervisionStub \
    MaestroPrebuilt \
    OdadPrebuilt \
    PartnerSetupPrebuilt \
    Phonesky \
    PixelLiveWallpaperPrebuilt \
    PixelSupportPrebuilt \
    PrebuiltBugle \
    RecorderPrebuilt \
    SafetyHubPrebuilt \
    ScribePrebuilt \
    SearchSelectorPrebuilt \
    SettingsIntelligenceGooglePrebuilt \
    SetupWizardPrebuilt \
    Showcase \
    TurboPrebuilt \
    Velvet \
    WallpaperEffect \
    WeatherPixelPrebuilt \
    WellbeingPrebuilt

# system/app
PRODUCT_PACKAGES += \
    GoogleExtShared \
    GooglePrintRecommendationService

# system/priv-app
PRODUCT_PACKAGES += \
    DocumentsUIGoogle \
    TagGoogle

# system_ext/app
PRODUCT_PACKAGES += \
    EmergencyInfoGoogleNoUi \
    Flipendo

# system_ext/priv-app
PRODUCT_PACKAGES += \
    GoogleFeedback \
    GoogleServicesFramework \
    NexusLauncherRelease \
    PixelSetupWizard \
    QuickAccessWallet \
    StorageManagerGoogle \
    TurboAdapter \
    WallpaperPickerGoogleRelease

# PrebuiltGmsCore
PRODUCT_PACKAGES += \
    PrebuiltGmsCoreSc \
    PrebuiltGmsCoreSc_AdsDynamite \
    PrebuiltGmsCoreSc_CronetDynamite \
    PrebuiltGmsCoreSc_DynamiteLoader \
    PrebuiltGmsCoreSc_DynamiteModulesA \
    PrebuiltGmsCoreSc_DynamiteModulesC \
    PrebuiltGmsCoreSc_GoogleCertificates \
    PrebuiltGmsCoreSc_MapsDynamite \
    PrebuiltGmsCoreSc_MeasurementDynamite \
    AndroidPlatformServices \
    MlkitBarcodeUIPrebuilt \
    VisionBarcodePrebuilt

# Safety Information
#PRODUCT_PACKAGES += \
#    SafetyRegulatoryInfo

$(call inherit-product, vendor/gms/product/blobs/product_blobs.mk)
$(call inherit-product, vendor/gms/system/blobs/system_blobs.mk)
$(call inherit-product, vendor/gms/system_ext/blobs/system-ext_blobs.mk)
